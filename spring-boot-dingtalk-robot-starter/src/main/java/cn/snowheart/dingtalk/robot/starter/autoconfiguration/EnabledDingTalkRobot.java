package cn.snowheart.dingtalk.robot.starter.autoconfiguration;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Import;

/**
 * DingTalk机器人的Enable注解
 * 自 1.0.1 版本起弃用该注解，采用spring.factories方式加载
 *
 * @author Wanxiang Liu
 * @version 1.0.0
 */
@Deprecated
@Configurable
@Import(DingTalkRobotAutoConfiguration.class)
public @interface EnabledDingTalkRobot {
}