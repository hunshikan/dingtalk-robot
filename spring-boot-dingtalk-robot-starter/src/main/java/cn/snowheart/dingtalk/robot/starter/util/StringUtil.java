package cn.snowheart.dingtalk.robot.starter.util;

/**
 * 针对String的util类
 *
 * @author Wanxiang Liu
 * @version 1.0.0
 */
public class StringUtil {

    /**
     * 判空
     *
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

    /**
     * 判非空
     *
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }
}